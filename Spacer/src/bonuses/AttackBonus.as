package bonuses
{
	import core.Assets;
	
	import starling.display.Image;
	import starling.display.Sprite;
	
	import states.Play;
	
	/**
	 * Bonus zwiekszajacy sile razenia hero
	 * 
	 * @private timeCreateObject Czas utworzenia bonusu
	 */
	public class AttackBonus extends Sprite
	{
		private var play:Play;
		private var timeCreateObject:uint;
		
		public function AttackBonus(play:Play, positionX:int, positionY:int)
		{
			var img :Image = new Image(Assets.ta.getTexture("attackBonus"));
			pivotX = img.width * 0.5;
			pivotY = img.height * 0.5;
			x = positionX;
			y = positionY;
			addChild(img);
			
			this.play = play;
			timeCreateObject = play.timerManager.getActuallyTime();
		}
		
		/**
		 * Update
		 * Sprawdza czy przypadkiem czas nie minal 10s
		 * jesli tak usuwa bonus
		 */
		public function update():void
		{
			//ile czasu juz jest na scenie
			var differenceTime: uint = play.timerManager.getActuallyTime() - timeCreateObject;
			
			//if (differenceTime % 2)
			alpha = 1 - (differenceTime/10) 
			
			if (differenceTime == 10)
			{
				destroy();
			}
		}
		
		/**
		 * Wywoluje sie gdy nastapi kolizja z hero
		 * zmienia bron na lepsza
		 */
		public function collisionWithHero():void
		{
			play.bulletManager.changeKindOfShootTo(1);
			
			destroy();
		}
		
		/**
		 * Usuwa obiekt
		 */
		public function destroy():void
		{
			var bonusesTable : Array = play.bonusesTable;
			for(var i: int= bonusesTable.length-1; i>=0; i--)
			{
				if (this == bonusesTable[i])
				{
					bonusesTable.splice(i, 1);
				}
			}
			
			this.removeFromParent(true);
		}
	}
}