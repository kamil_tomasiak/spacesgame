package core
{

	import interfaces.IState;
	
	import services.PhpService;
	
	import starling.display.Sprite;
	import starling.events.Event;
	
	import states.GameOver;
	import states.Menu;
	import states.Play;
	
	/**
	 * Glowna klasa gry
	 * 
	 * @public static const MENU_STATE Numer stanu gry Menu
	 * @public static const PLAY_STATE Numer stanu gry Play
	 * @public static const GAME_OVER_STATE Numer stanu gry Game over
	 * @public score Ilosc punktow zdobytych przez gracza
	 */
	public class Game extends Sprite
	{
		//3 mozliwe stany rozgrywki = przypisane do nich numery
		public static const MENU_STATE:int = 0;
		public static const PLAY_STATE:int = 1;
		public static const GAME_OVER_STATE:int = 2;
		public var score:uint = 0;
		
		//obecny staus rozgrywki
		private var current_state:IState;
		
		//konstruktor
		public function Game()
		{
			Assets.init();
			PhpService.init();
			addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(event:Event):void
		{
			//ustawinie statusu na poczatkowy stan
			changeState(MENU_STATE);
			addEventListener(Event.ENTER_FRAME, update);
		}
		
		/**
		 * Metoda sluzacza do zmiany obecnego stanu gry
		 * 
		 * @param state Numer statusu na ktory ma zmienic.
		 */
		public function changeState(state:int):void
		{
			//obiekt wczesniejszego statusu zostaje zniszczony
			if(current_state != null)
			{
				current_state.destroy();
				current_state = null;
			}
			
			switch(state)
			{
				case MENU_STATE:
					current_state = new Menu(this);
					break;
				
				case PLAY_STATE:
					score = 0;
					current_state = new Play(this);
					break;
				
				case GAME_OVER_STATE:
					current_state = new GameOver(this);
					break;
			}
			
			//obecny status zostaje dodany do drzewa renderowania
			addChild(Sprite(current_state));
		}
		
		/**
		 * Metoda ta wywoluje metode update obecnego statusu
		 */
		private function update(event:Event):void
		{
			current_state.update();
		}
		
		/**
		 * Dodaje ilosc punktow do wyniku
		 */
		public function addScore(numberOfPoints: uint):void
		{
			score += numberOfPoints;
		}
		
		/**
		 * Ilosc zdobytych punktow
		 * 
		 * @return ilosc punktow
		 */
		public function getScore():uint
		{
			return score;
		}
	}
}