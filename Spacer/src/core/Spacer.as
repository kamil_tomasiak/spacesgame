package core
{
	import flash.display.Sprite;
	
	import starling.core.Starling;
	
	//szerokosc, wysokosc okna, ilosc klatek na sekunde
	[SWF(width=700, height=600, frameRate=60, backgroundColor=0x000000)]
	
	/**
	 * Klasa odpowiedzialna za "polaczenie" flasha i starling framework
	 */
	public class Spacer extends Sprite
	{
		public function Spacer()
		{
			var star:Starling = new Starling(Game, stage);
			star.showStats = true;
			star.start();
		}
	}
}