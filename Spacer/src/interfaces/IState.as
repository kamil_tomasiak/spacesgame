package interfaces
{
	/**
	 * Interfejs implementowany przez stany rozgrywki
	 */
	public interface IState
	{
		function update():void;
		function destroy():void;
	}
}