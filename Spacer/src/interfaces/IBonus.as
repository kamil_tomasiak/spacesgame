package interfaces
{
	/**
	 * Interfejs dla bonusow
	 */
	public interface IBonus
	{
		function collisionWithHero():void;
	}
}