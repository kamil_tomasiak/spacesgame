package interfaces
{
	/**
	 * Interfejs implemetowany przez klasy wrogow
	 */
	public interface IEnemy
	{
		function update():void;
		function destroy():void;
		function decreaseLives():void;
		function getCountLives():uint;
		function destroyDress():void;
		
		function getDressX():int;
		function getDressY():int;
		function getDressPivotX():int;
		function getDressPivotY():int;
		function isItKilled():Boolean;
	}
}