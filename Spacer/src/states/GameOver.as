package states
{
	import core.Assets;
	import core.Game;
	
	import interfaces.IState;
	
	import objects.Background;
	
	import starling.display.Button;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.text.TextField;
	
	/**
	 * Obsługa statusu GameOver
	 * 
	 * @private game Glowna klasa gry
	 * @private background Tlo gry
	 * @private overText Napis game over
	 * @private tryAgain przycisk do uruchomenia gry jeszcze raz
	 * @private scoreText Ilosc punktow wyswietlona na ekranie
	 */
	public class GameOver extends Sprite implements IState
	{
		private var game:Game;
		private var background:Background;
		private var overText:TextField;
		private var tryAgain:Button;
		private var scoreText:TextField;
		
		/**
		 * Konstruktor
		 */
		public function GameOver(game:Game)
		{
			this.game = game;
			addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		/**
		 * Inicjalizacja tla, napisu i przycisku
		 */
		private function init(event:Event):void
		{
			background = new Background();
			addChild(background);
			
			overText = new TextField(800, 200, "GAME OVER", "KomikaAxis", 72, 0xFFFFFF);
			overText.hAlign = "center";
			overText.pivotX = overText.width * 0.5;
			overText.x = 350;
			overText.y = 150;
			addChild(overText);
			
			var points : String = "WYNIK: ".concat(game.getScore().toString(), " MOENT");
			scoreText = new TextField(800, 200, points, "KomikaAxis", 52, 0xFFFFFF);
			scoreText.hAlign = "center";
			scoreText.pivotX = scoreText.width * 0.5;
			scoreText.x = 350;
			scoreText.y = 250;
			addChild(scoreText);
			
			tryAgain = new Button(Assets.ta.getTexture("tryAgainButton"));
			tryAgain.addEventListener(Event.TRIGGERED, onAgain);
			tryAgain.pivotX = tryAgain.width * 0.5;
			tryAgain.x = 350;
			tryAgain.y = 450;
			addChild(tryAgain);
		}
		
		/**
		 * Dzialanie po nacisnieciu przycisku
		 * przejscie do stanu rozgrywki
		 */
		private function onAgain(exent:Event):void
		{
			tryAgain.removeEventListener(Event.TRIGGERED, onAgain);
			game.changeState(Game.PLAY_STATE);
		}
		
		/**
		 * Update tla
		 */
		public function update():void
		{
			background.update();
		}
		
		/**
		 * Usuniecie obiektu
		 */
		public function destroy():void
		{
			removeFromParent(true);
		}
	}
}