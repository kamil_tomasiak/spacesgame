package states
{
	import core.Assets;
	import core.Game;
	
	import interfaces.IState;
	
	import objects.Background;
	
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	
	/**
	 * Obsluga stanu gry -> Menu
	 * 
	 * @private game Glowny obiekt gry
	 * @private background Tlo tego stanu
	 * @private logo Obraz wyswietlany w tym stanie
	 * @private play Przycisk przenoszacy do rozgrywki
	 */
	public class Menu extends Sprite implements IState
	{
		private var game:Game;
		private var background:Background;
		private var logo:Image;
		private var play:Button;
		
		/**
		 * Konstruktor
		 * 
		 * @param game Glowny obiekt gry
		 */
		public function Menu(game:Game)
		{
			this.game = game;
			addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		/**
		 * Inicjalizacja tla, loga i buttonu
		 */
		private function init(event:Event):void
		{
			background = new Background();
			addChild(background);
			
			logo = new Image(Assets.ta.getTexture("logo"));
			logo.pivotX = logo.width * 0.5;
			logo.x = 350;
			logo.y = 250;
			addChild(logo);
			
			play = new Button(Assets.ta.getTexture("playButton"));
			play.addEventListener(Event.TRIGGERED, onPlay);
			play.pivotX = play.width * 0.5;
			play.x = 350;
			play.y = 450;
			addChild(play);
		}
		
		/**
		 * Zmiana stanu
		 * Przejscie do rozgrywki
		 */
		private function onPlay(event:Event):void
		{
			game.changeState(Game.PLAY_STATE);
		}
		
		/**
		 * Update tla
		 */
		public function update():void
		{
			background.update();
		}
		
		/**
		 * Usuniecie tego stanu
		 */
		public function destroy():void
		{
			background.removeFromParent(true);
			background = null;
			logo.removeFromParent(true);
			logo = null;
			play.removeFromParent(true);
			play = null;
			removeFromParent(true);
		}
	}
}