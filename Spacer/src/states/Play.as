package states
{
	import com.leebrimelow.starling.StarlingPool;
	
	import core.Game;
	
	import enemiesSpace.NormalEnemy;
	import enemiesSpace.ShooterEnemy;
	
	import flash.display.Stage;
	import flash.events.MouseEvent;
	import flash.utils.Timer;
	
	import interfaces.IEnemy;
	import interfaces.IState;
	
	import managers.BulletManager;
	import managers.CollisionManager;
	import managers.ExplosionManager;
	import managers.TimerManager;
	
	import objects.Background;
	import objects.Heart;
	import objects.Hero;
	import objects.NormalEnemyDress;
	import objects.Score;
	import objects.ShooterEnemyDress;
	import objects.SupplierEnemyDress;
	import objects.TimerText;
	
	import services.PhpService;
	
	import starling.core.Starling;
	import starling.display.Sprite;
	import starling.events.Event;
	
	/**
	 * Obsluga calej rozgrywki
	 *
	 * @public game Glowny obiekt gry
	 * @private backgorund Tlo gry
	 * @public hero Glowna postac gry
	 * @public bulletManager Manager strzalow hero
	 * @public fire True jesli hero strzela
	 * @private ns Scena
	 * @private collisionManager Manager kolizji
	 * @public explosionManager Manager eksplozji
	 * @public score Wynik, ilosc punktow zdobyta
	 * @public poolForNormalEnemyDress Pula kostiumow normalnych wrogow
	 * @public enemiesNormal Tablica wszystkich noramlnych wrogow
	 * @private count Ilosc klatek wyswietlonych do tej pory
	 * @public spaceShipsFromServer Tablica statkow (pobrana z serwera)
	 * @public timeText Czas wyswietlony na ekranie
	 * @private timeManager Timer zarzadzajacy czasem
	 * @public heart Zycie hero
	 * @private afterInit Do zabezpieczenia sie, najpierw dane musza byc odczytane z serwera
	 * @public poolForShooterEnemyDress Pula kostiumow strzelajacych wrogow
	 * @public enemiesShooter Tablica strzelajacych wrogow
	 * @public poolForSupplierEnemyDress Pula worgow dostarczajacych
	 * @public enemiesSupplier Tablica dostarczajacych wrogow
	 * @public bonusesTable Tablica bonusow ktore znajduja sie na scenie
	 */
	public class Play extends Sprite implements IState
	{
		public var game:Game;
		private var background:Background;
		public var hero:Hero;
		public var bulletManager:BulletManager;
		public var fire:Boolean = false;
		private var ns:Stage;
		private var collisionManager:CollisionManager;
		public var explosionManager:ExplosionManager;
		public var score:Score;
		
		public var poolForNormalEnemyDress:StarlingPool;
		public var enemiesNormal:Array; 
		
		private var count:int = 0;
		
		public var spaceShipsFromServer:Array;
		public var timeText:TimerText;
		public var timerManager:TimerManager;
		public var heart:Heart;
		private var afterInit : Boolean= false;
		
		public var poolForShooterEnemyDress:StarlingPool;
		public var enemiesShooter:Array;
		public var poolForSupplierEnemyDress:StarlingPool;
		public var enemiesSupplier:Array;
		public var bonusesTable:Array;
		
		/**
		 * Konstruktor
		 * 
		 * @param game Glowny obiekt gry
		 */
		public function Play(game:Game)
		{
			PhpService.getTableOfSpacesRequest(this);
			
			this.game = game;
			touchable = false; //optymalizaja wylaczenie rekacji na klikanie
			//addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		/**
		 * Inicjalizajca wszytskich obiektow dostepnych podczas rogzgrywki
		 */
		private function init():void
		{
			ns = Starling.current.nativeStage;
			
			background = new Background();
			addChild(background);
			
			hero = new Hero(this);
			addChild(hero);
			
			score = new Score(this);
			addChild(score);
			score.x = 450;
			
			//odmiezanie czasu
			timeText = new TimerText();
			addChild(timeText);
			timeText.x = 10;
			
			//zarzadzanie dodawaniem statkow
			timerManager = new TimerManager(this);
			
			//zycie hero
			heart = new Heart(this);
			
			//pool dla wrogow
			poolForNormalEnemyDress = new StarlingPool(NormalEnemyDress, 40);
			poolForShooterEnemyDress = new StarlingPool(ShooterEnemyDress, 40);
			poolForSupplierEnemyDress = new StarlingPool(SupplierEnemyDress, 40);
			
			//tablica wrogownormalnych
			enemiesNormal = new Array();
			
			//tablica strzelajacych wrogow
			enemiesShooter = new Array();
			
			//tablica dostarczajacyhc wrogow
			enemiesSupplier = new Array();
			
			//stare do przejrzenia
			bulletManager = new BulletManager(this);
			
			collisionManager = new CollisionManager(this);
			explosionManager = new ExplosionManager(this);
			
			//nasluchiwanie na klikniecie myszka
			ns.addEventListener(MouseEvent.MOUSE_DOWN, onDown);
			ns.addEventListener(MouseEvent.MOUSE_UP, onUp);
			
			//tablica bonusow ktore znajduja sie na scenie
			bonusesTable = new Array();
			
			afterInit = true;
		}
		
		/**
		 * Pobranie danych z serwera i zapisanie ich do tablicy
		 * dopiero po tej czynnosci moze byc wykonany init
		 */
		public function getTableOfSpacesResponse(spaceShips:Array):void
		{
			spaceShipsFromServer = spaceShips;
			init();
		}
		
		/**
		 * Wykonuje sie gdy przycisk myszy jest wciskany
		 * uruchamia strzelanie hero
		 */
		private function onDown(evet:MouseEvent):void
		{
			fire = true;
		}
		
		/**
		 * Wykonuej sie gdy przycisk zostaje puszczony
		 * wylacza strzelanie hero
		 */
		private function onUp(evet:MouseEvent):void
		{
			fire = false;
			bulletManager.count = 0;
		}
		
		/**
		 * Update 
		 * Dopiero po inicie zostaje to uruchomione
		 * Robi update dla wszytskich obiektow z rozgrywki
		 */
		public function update():void
		{
			if (afterInit)
			{
				count++;
				
				var i:int;
				var len:int;
				//update normalnych wrogow 
				var enemy:IEnemy;
				len= enemiesNormal.length;
				for (i=len-1; i>=0; i--)
				{
					enemy = enemiesNormal[i];
					enemy.update();
				}
				
				//update strzelajacych wrogow
				len= enemiesShooter.length;
				for (i=len-1; i>=0; i--)
				{
					enemy = enemiesShooter[i];
					enemy.update();
				}
				
				//update dostarczajacych wrogow
				len= enemiesSupplier.length;
				for (i=len-1; i>=0; i--)
				{
					enemy = enemiesSupplier[i];
					enemy.update();
				}
				
				//update wszytskich bonusow
				len = bonusesTable.length;
				for (i= len-1; i>=0; i--)
				{
					bonusesTable[i].update();
				}
				
				background.update();
				hero.update();
				bulletManager.update();
				collisionManager.update();
				
				
			}
		}
		
		/**
		 * Usuniecie stanu rozgrywki
		 */
		public function destroy():void
		{
			//zatrzymanie timera
			timerManager.stopTimer();
			timerManager = null;
			
			ns.removeEventListener(MouseEvent.MOUSE_DOWN, onDown);
			ns.removeEventListener(MouseEvent.MOUSE_UP, onUp);
			bulletManager.destroy();
			
			//usuniecie normalnych wrogow ze sceny
			var i:int;
			var enemy:IEnemy;
			for (i=0; i<enemiesNormal.length; i++) 
			{
				enemy = enemiesNormal[i];
				enemy.destroy();	
			}
			
			//usuniecie wrogow strzelajacych
			for (i=0; i<enemiesShooter.length; i++) 
			{
				enemy = enemiesShooter[i];
				enemy.destroy();	
			}
			
			poolForNormalEnemyDress.destroy();
			poolForShooterEnemyDress.destroy();
			poolForNormalEnemyDress = null;
			poolForShooterEnemyDress = null;
			
			spaceShipsFromServer=null;
			//enemiesNormal = null;
			//enemiesShooter = null;
			
			removeFromParent(true);
		}
	}
}