package services
{
	import flash.net.NetConnection;
	import flash.net.Responder;

	/**
	 * Klasa odpowiedzialna za polaczenie z serwerem
	 * 
	 * @private connection Polaczenie z serwerem
	 * @private responder Odbiorca wiadomosci od serwera 
	 */
	public class PhpService
	{
		private static var connection:NetConnection;
		private static var responder:Responder;
		
		/**
		 * Zainicjowanie polaczenia z serwerem
		 */
		public static function init():void
		{
			connection = new NetConnection();
			connection.connect("http://localhost:83/amfphp-2.2/Amfphp/index.php");
		}//http://strona2.home.pl/TESTY/amfphp-2.2/Amfphp/index.php
		//http://localhost:83/amfphp-2.2/Amfphp/index.php
		
		/**
		 * Odczytywanie danych z serwera
		 * wywolanie metody getTableOfSpaces po stronie serwera
		 */
		public static function getTableOfSpacesRequest(delegate:Object):void
		{
			responder = new Responder(delegate.getTableOfSpacesResponse, null);
			connection.call("SpaceGameService/getTableOfSpaces", responder);
		}
		
	}
}