package objects
{
	import core.Assets;
	
	import starling.display.Image;
	import starling.display.Sprite;
	
	/**
	 * Textura strzalu wroga
	 */
	public class BulletEnemy extends Sprite
	{
		public function BulletEnemy()
		{
			var img:Image = new Image(Assets.ta.getTexture("bulletEnemy"));
			pivotX = img.width * 0.5;
			pivotY = img.height * 0.5;
			addChild(img);
		}
	}
}