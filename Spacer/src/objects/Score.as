package objects
{
	import starling.display.Sprite;
	import starling.text.TextField;
	
	import states.Play;
	
	/**
	 * Punkty zdobyte przez uzytkownika
	 * 
	 * @private score Tekst wyswietlany na ekranie
	 */
	public class Score extends Sprite
	{
		private var score:TextField;
		private var play:Play;
	
		/**
		 * Konstruktor
		 * 
		 * @param play Status gry gdzie sie to pokazuje
		 */
		public function Score(play: Play)
		{
			score = new TextField(300, 100, "0", "KomikaAxis", 32, 0xFFFFFF);
			score.hAlign "left";
			addChild(score);
			
			this.play = play;
		}
		
		/**
		 * Metoda zwieksza ilosc punktow
		 * 
		 * @param amt O ile trzeba zwiekszyc dotychczasowa ilosc punktow
		 */
		public function addScore(amt:uint):void
		{
			//dodaje punkty
			play.game.addScore(amt);
			//pokazuje na ekranie
			score.text = (parseInt(score.text) + amt).toString();
		}
	}
}