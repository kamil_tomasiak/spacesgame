package objects
{
	import core.Assets;
	
	import starling.display.BlendMode;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.text.TextField;
	
	import states.Play;
	
	/**
	 * Klasa zycia hero
	 * 
	 * @public heartImage Obraz zesrca wyswietlany na planszy
	 * @public countLivesText Ilosc zycia, tekst wyswietlany w sercu
	 * @private play Stan w ktorym to serce jest wyswietlane
	 */
	public class Heart extends Sprite
	{
		public var heartImage:Image;
		public var countLivesText:TextField;
		private var play:Play;
		
		/**
		 * Konstruktor
		 * 
		 * @param play Stan w ktorym to serce jest wyswietlane
		 */
		public function Heart(play:Play)
		{
			this.play = play;
			
			//obrazek serca
			heartImage = new Image(Assets.ta.getTexture("heart"));
			heartImage.pivotX = 0.1;
			heartImage.pivotY = 0.1;
			heartImage.x = 25;
			heartImage.y = 100;
			play.addChild(heartImage);
						
			//stworzenie napisu
			countLivesText = new TextField(60, 60, "3", "KomikaAxis", 23, 0xFFFFFF); 
			countLivesText.pivotX = 0.1;
			countLivesText.pivotY = 0.1;
			countLivesText.x = heartImage.x-10;
			countLivesText.y = heartImage.y-15;
			play.addChild(countLivesText);
		}
		
		/**
		 * Zmniejszenie ilosci zycia wyswietlanego na ekranie
		 * wartosc zycia pobrana z obiektu hero
		 */
		public function updateHeart():void
		{
			countLivesText.text = String(play.hero.getCountLives());	
		}
		
	}
}