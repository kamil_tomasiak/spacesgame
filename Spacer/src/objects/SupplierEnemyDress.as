package objects
{
	import core.Assets;
	
	import starling.display.Image;
	import starling.display.Sprite;
	
	/**
	 * Textura dla worga zostawiajacego niespodzianki
	 */
	public class SupplierEnemyDress extends Sprite
	{
		public function SupplierEnemyDress()
		{
			var img :Image = new Image(Assets.ta.getTexture("supplierEnemy"));
			pivotX = img.width * 0.5;
			pivotY = img.height * 0.5;
			addChild(img);
		}
	}
}