package objects
{
	import core.Assets;
	
	import starling.display.Image;
	import starling.display.Sprite;
	
	/**
	 * Textura wroga strzelajacego
	 */
	public class ShooterEnemyDress extends Sprite
	{
		public function ShooterEnemyDress()
		{
			var img: Image = new Image(Assets.ta.getTexture("shooterEnemy"));
			pivotX = img.width * 0.5;
			pivotY = img.height * 0.5;
			addChild(img);
		}
	}
}