package objects
{
	import core.Assets;
	
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.extensions.PDParticleSystem;
	
	import states.Play;
	
	/**
	 * Glowna postac gry
	 * 
	 * @private play Stan w ktorym wystepuje hero
	 * @private smoke Animacja dymu wylatujacego z tylu statku
	 * @public lives Ilosc zyc ktore posiada hero
	 * @private kindOfMouseMovement Rodzaj w jaki porusza sie statek podczas ruchu myszka
	 */
	public class Hero extends Sprite
	{
		private var play:Play;
		private var smoke:PDParticleSystem;
		public var lives:int =3; 
		
		private var kindOfMouseMovement:int = 5;
		
		/**
		 * Konstruktor
		 * 
		 * @param play Stan w ktorym wystepuje hero
		 */
		public function Hero(play:Play)
		{
			this.play = play;
		
			//zaladowanie obrazka
			var img:Image = new Image(Assets.ta.getTexture("hero"));
			pivotX = img.width * 0.5;
			pivotY = img.height * 0.5;
			addChild(img);
			
			//stworzenie dymku dla hero
			smoke = new PDParticleSystem(XML(new Assets.smokeXML()),
				Assets.ta.getTexture("smoke"));
			Starling.juggler.add(smoke);
			play.addChild(smoke);
		}
		
		/**
		 * Update pozycji hero
		 * w zaleznosci od ruchu myszka
		 * od razu jest update dla dymku robiony
		 */
		public function update():void
		{
			smoke.emitterX = x;
			smoke.emitterY = y + 20;
			trace("wynik: " + (kindOfMouseMovement/10));
			x += (Starling.current.nativeStage.mouseX - x) * (kindOfMouseMovement/10);			
			y += (Starling.current.nativeStage.mouseY - y) * (kindOfMouseMovement/10);
			
			//jesli statkowi sie polepszylo dym znika
			if(kindOfMouseMovement >= 5) 
			{
				smoke.stop();
			}
			else
			{
				smoke.start();
			}
		}
		
		/**
		 * Polepsza poruszanie statku myszka
		 * dodaje 1 
		 */
		public function improvementKindOfMouseMovement():void
		{
			if(kindOfMouseMovement < 10)
				kindOfMouseMovement += 1;
		}
		
		/**
		 * pogorszenie poruszania statku myszka reaguje gozej
		 * zmienjsza 0.1, po stracie zycia
		 */
		public function deteriorationKindOfMouseMovement():void
		{
			if(kindOfMouseMovement > 1)
				kindOfMouseMovement -= 1;
		}
		
		
		/**
		 * Zmniejszanie ilosci zycia hero
		 * zmiana broni na najgorsza
		 */
		public function decreaseHeart():void
		{
			lives--;
			
			//powrot do najprostszej bronii i pogorszenie poruszania
			play.bulletManager.changeKindOfShootTo(0);
			deteriorationKindOfMouseMovement();
		}
		
		/**
		 * Ilosc zycia hero
		 * 
		 * @return ilosc zycia (int)
		 */
		public function getCountLives():int
		{
			return lives;
		}
		
		/**
		 * Dodaje zycie dla hero
		 */
		public function addLives():void
		{
			lives++;
		}
	}
}