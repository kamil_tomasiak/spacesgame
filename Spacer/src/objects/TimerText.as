package objects
{
	import starling.display.Sprite;
	import starling.text.TextField;
	
	/**
	 * Klasa czasu dostepnego w grze
	 *
	 * @private timeText Napis wyswietlany na scenie
	 * @private minutes Ilosc minut dostepnych w grze
	 * @private seconds Ilosc sekund
	 */
	public class TimerText extends Sprite
	{
		private var timeText:TextField;
		private var minutes:int = 4;
		private var seconds:int = 0;
		
		/**
		 * Konstruktor
		 */
		public function TimerText()
		{
			timeText = new TextField(80, 100, minutes.toString()+":00", "KomikaAxis", 32, 0xFFFFFF);
			timeText.hAlign "right";
			addChild(timeText);
		}
		
		/**
		 * Ilosc czasu zmniejsza sie o jedna sekunde
		 * odpowiendie wyswietlenie na ekranie
		 */
		public function tickTime():void
		{
			if(seconds == 0)
			{
				minutes -= 1;
				seconds = 59;
			} 
			else
			{
				seconds -=1;
			}
			
			timeText.text = minutes.toString() + ":";
			if (seconds < 10)
			{
				timeText.text = timeText.text + "0";
			}
			timeText.text += seconds.toString();
			
		}
	}
}