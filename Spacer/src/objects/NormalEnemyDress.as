package objects
{
	import core.Assets;
	
	import enemiesSpace.NormalEnemy;
	
	import interfaces.IEnemy;
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.textures.Texture;
	
	import states.Play;
	
	/**
	 * Texture normalnego wroga
	 */
	public class NormalEnemyDress extends Sprite//extends MovieClip
	{
		public function NormalEnemyDress()
		{
			var img: Image = new Image(Assets.ta.getTexture("normalEnemy"));//, 12);
			pivotX = img.width * 0.5;
			pivotY = img.height * 0.5;
			addChild(img);
		}
	}
}