package managers
{
	import core.Game;
	
	import enemiesSpace.NormalEnemy;
	import enemiesSpace.ShooterEnemy;
	import enemiesSpace.SupplierEnemy;
	
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	import states.Play;

	/**
	 * Timer odliczajacy czas rozgrywki
	 * W odpowiedniej sekundzie wypuszcza statki
	 * 
	 * @private timer Timer odliczajacy czas
	 * @private play Stan rozgrywki w ktorym zostaje umieszczony
	 */
	public class TimerManager
	{
		private var timer:Timer;
		private var play:Play;
		
		/**
		 * Konstruktor
		 * 
		 * @param play Stan rozgrywki w ktorym zostaje uruchomiony
		 */
		public function TimerManager(play:Play)
		{
			this.play = play;
			//Timer co ile milisekund ma sie wykonywac (1s), i ile razy.
			timer = new Timer(1000, 240);
			timer.addEventListener(TimerEvent.TIMER, timerHandler);
			timer.addEventListener(TimerEvent.TIMER_COMPLETE, completeHandler);
			timer.start();
		}
		
		/**
		 * Wykonuje sie po zakonczeniu odliczania
		 */
		protected function completeHandler(event:TimerEvent):void
		{
			play.game.changeState(Game.GAME_OVER_STATE); 
		}
		
		/**
		 * Metoda wykonywana przez timer
		 * Ustalone wczesniej ze co 1s
		 */
		protected function timerHandler(event:TimerEvent):void
		{
			//zegar tyka dalej
			play.timeText.tickTime();
			
			//kod ktory w odpowiednim czasie tworzy statki odczytujac tablice z serwera
			var spacesFromServer:Array = play.spaceShipsFromServer;
			var len:int = spacesFromServer.length;
			var enemiesNormal:Array = play.enemiesNormal;
			var enemiesShooter:Array = play.enemiesShooter;
			var enemiesSupplier:Array = play.enemiesSupplier;
			
			//dopoki czas sie zgadza, beda z serwera tworzone nowe statki
			while (len>0 && spacesFromServer[0][0] == timer.currentCount)
			{
				//jesli typ statku sie zgadza to tworze
				if (spacesFromServer[0][1] == 1) 
				{
					var newNormalEnemy:NormalEnemy = new NormalEnemy(play, spacesFromServer[0][2], spacesFromServer[0][3]);
					enemiesNormal.push(newNormalEnemy);
				}
				else if (spacesFromServer[0][1] == 2)
				{
					var newShooterEnemy:ShooterEnemy = new ShooterEnemy(play, spacesFromServer[0][2], spacesFromServer[0][3]);
					enemiesShooter.push(newShooterEnemy);
				}
				else if (spacesFromServer[0][1] == 3)
				{
					var newSupplierEnemy:SupplierEnemy = new SupplierEnemy(play, spacesFromServer[0][2], spacesFromServer[0][3], spacesFromServer[0][4]);
					enemiesSupplier.push(newSupplierEnemy);
				}
				
				//usuwam statek z tablicy serwerowej
				spacesFromServer.splice(0,1);
				len--;
			}
		}
		
		/**
		 * Zwraca aktualny czas na timerze
		 * ile sekund juz uplynelo
		 */
		public function getActuallyTime():uint
		{
			return timer.currentCount;
		}
		
		/**
		 * Metoda zatrzymujaca timer
		 */
		public function stopTimer():void
		{
			timer.stop();
		}
	}
}