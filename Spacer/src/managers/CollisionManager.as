package managers
{
	import core.Assets;
	import core.Game;
	
	import enemiesSpace.NormalEnemy;
	import enemiesSpace.ShooterEnemy;
	
	import flash.geom.Point;
	
	import interfaces.IEnemy;
	
	import objects.Bullet;
	import objects.BulletEnemy;
	import objects.Hero;
	import objects.NormalEnemyDress;
	
	import states.Play;

	/**
	 * Klasa obslugjaca kolizje obiektow
	 * 
	 * @private play Stan gry w ktorym badane sa kolizje
	 * @private p1 Punkt srodkowy elementu 1 podejrzanego o kolizje
	 * @private p2 Punkt srodkowy elementu 2 podejrzanego o kolizje
	 * @private count Optymalizajca
	 */
	public class CollisionManager
	{
		private var play:Play;
		private var p1:Point = new Point();
		private var p2:Point = new Point();
		//OPTYMALIZACJA
		private var count:int = 0;
		
		/**
		 * Konstruktor
		 * 
		 * @param play Stan w ktorym dziala
		 */
		public function CollisionManager(play:Play)
		{
			this.play = play;
		}
		
		/**
		 * Update
		 * Wywoluje metody ktore sprawdzaja rozne kolizje
		 */
		public function update():void
		{
			//OPTYMALIZACJA
			if(count & 1)
			{
				bulletsAndAliens();
				heroAndEnemyBullets();
			}
			else
			{
				heroAndAliens();
				heroAndBonus();
				//heroAndEnemyBullets();
			}
				
			count++;
		}

		/**
		 * Wykrywa kolizje pomiedzy hero i wszytskimi bonusami ze sceny
		 */
		private function heroAndBonus():Boolean
		{
			var bonusesTable:Array = play.bonusesTable;
			//var enemy:IEnemy;
			
			for(var i:int=bonusesTable.length-1; i>=0; i--)
			{
				//jezeli nie jest zestrzelony to spradzamy zderzenie
					p1.x = play.hero.x;
					p1.y = play.hero.y;
					p2.x = bonusesTable[i].x;
					p2.y = bonusesTable[i].y;
					
					//wykrywanie kolizji
					if(Point.distance(p1, p2) < bonusesTable[i].pivotY + play.hero.pivotY)
					{
						if (play.hero.lives < 2)
						{
							play.game.changeState(Game.GAME_OVER_STATE);
						}
						else
						{	
							bonusesTable[i].collisionWithHero(); //usuniecie wroga
						}
						//znalazl zderzenie to nie ma co dalej sprawdzac
						return true;
					}
			}
			return false;
		}
		
		/**
		 * Kolizja pomiedzy hero i wrogami
		 * wywoluje odpowiednie metody
		 * Zderzenie z NormalEnemy, ShooterEnemy
		 */
		private function heroAndAliens():void
		{
			//jesli nie znaleziono zderzenia hero i normalny to sprawdzam strzelajacych
			if(!(heroAndEnemy(play.enemiesNormal)))
			{
				if(!heroAndEnemy(play.enemiesShooter))
				{
					heroAndEnemy(play.enemiesSupplier);
				}
			}
		}
		
		/**
		 * Obsluguje kolizje dla dwoch rodzajow statkow
		 * pomiedzy statkami i hero
		 * 
		 * @param enemies Tablica wrogow dal ktorych kolizja ma byc badana
		 */
		private function heroAndEnemy(enemies:Array):Boolean
		{
			var enemies:Array = enemies;
			var enemy:IEnemy;
			
			for(var i:int=enemies.length-1; i>=0; i--)
			{
				enemy = enemies[i];
				//jezeli nie jest zestrzelony to spradzamy zderzenie
				if (!enemy.isItKilled())
				{
					p1.x = play.hero.x;
					p1.y = play.hero.y;
					p2.x = enemy.getDressX();
					p2.y = enemy.getDressY();
					
					//wykrywanie kolizji
					if(Point.distance(p1, p2) < enemy.getDressPivotY() + play.hero.pivotY)
					{
						if (play.hero.lives < 2)
						{
							play.game.changeState(Game.GAME_OVER_STATE);
						}
						else
						{
							//zmienjszanie zycia hero i ilosci zycia na ekranie
							play.hero.decreaseHeart();
							play.heart.updateHeart();
							
							//eksplozja do zniszczenia wroga
							Assets.explosion.play();
							play.explosionManager.spawn(enemy.getDressX(), enemy.getDressY());
							enemy.destroyDress(); //usuniecie wroga
						}
						//znalazl zderzenie to nie ma co dalej sprawdzac
						return true;
					}
				}
			}
			return false;
		}
		
		/**
		 * Wykrwa kolizje pomiedzy hero i pociskami 
		 * wroga ShooterEnemy
		 */
		private function heroAndEnemyBullets():void
		{
			var enemiesShooter:Array = play.enemiesShooter;
			
			var len:int = enemiesShooter.length;
			for(var j:int=0; j<len; j++)
			{
				var enemy : ShooterEnemy = enemiesShooter[j];
				var bullets: Array = enemiesShooter[j].bullets;
				for(var i:int=bullets.length-1; i>=0; i--)
				{
					var bullet: BulletEnemy = bullets[i];
					p1.x = play.hero.x;
					p1.y = play.hero.y;
					p2.x = bullet.x;
					p2.y = bullet.y;
					
					if(Point.distance(p1, p2) < bullet.pivotY + play.hero.pivotY)
					{
						if (play.hero.lives < 2)
						{
							play.game.changeState(Game.GAME_OVER_STATE);
						}
						else
						{
							play.hero.decreaseHeart();
							play.heart.updateHeart();
							
							//eksplozja do zniszczenia wroga
							play.explosionManager.spawn(bullet.x, bullet.y);
							
							//usuniecie  pocisku ktory zniszczyl hero
							enemy.destroyBullet(i, bullet);
						}
						i=-1; //znalazl zderzenie to nie ma co dalej sprawdzac
						j=len;
					}
				}
			}
		}
		
		/**
		 * Wywoluje metody odpowiedzialne za wykrywanie kolizji pomiedzy
		 * pociskami hero i wrogami
		 */
		private function bulletsAndAliens():void
		{
			bulletsHeroAndEnemy(play.enemiesNormal);
			bulletsHeroAndEnemy(play.enemiesShooter);
			bulletsHeroAndEnemy(play.enemiesSupplier);
		}
		
		/**
		 * Wykrywa kolizje pomiedzy pociskami hero a wrogiem
		 * 
		 * @param enemies Tablica wrogow ktorych kolizje maja zostac wykryte
		 */
		private function bulletsHeroAndEnemy(enemies:Array):void
		{
			var ba:Array = play.bulletManager.bullets;
			
			var enemies:Array = enemies;
			var b:Bullet;
			var enemy:IEnemy;
			
			for(var i:int=ba.length-1; i>=0; i--)
			{
				b = ba[i];
				p1.x = b.x;
				p1.y = b.y;
				
				var j:int;
				for(j=enemies.length-1; j>=0; j--)
				{	
					enemy = enemies[j];
					if(!enemy.isItKilled())
					{
						p2.x = enemy.getDressX();
						p2.y = enemy.getDressY();
						if(Point.distance(p1, p2) < enemy.getDressPivotY() + b.pivotY)
						{
							enemy.decreaseLives();
							
							//zmienjaszm ilosc zycia dla wroga
							if (enemy.getCountLives() == 0)
							{
								enemy.destroyDress();
								Assets.explosion.play();
								play.explosionManager.spawn(enemy.getDressX(), enemy.getDressY());
							}
							play.bulletManager.destroyBullet(b);
							play.score.addScore(10);
						}
					}
				}
			}
		}
		
	}
}