package managers
{
	import com.leebrimelow.starling.StarlingPool;
	
	import objects.Explosion;
	
	import starling.core.Starling;
	import starling.events.Event;
	
	import states.Play;

	/**
	 * Klasa zarzadzajaca eksplozjami
	 * 
	 * @private play Stan w ktorym maja byc pokazywane
	 * @private pool Pula dla kolizji
	 */
	public class ExplosionManager
	{
		private var play:Play;
		private var pool:StarlingPool;
		
		/**
		 * Konstruktor
		 * 
		 * @param play Stan w ktorym ma zostac wyswietlony
		 */
		public function ExplosionManager(play:Play)
		{
			this.play = play;
			pool = new StarlingPool(Explosion, 15);
		}
		
		/**
		 * Utorzenie nowej eksplozji
		 * 
		 * @param x Pozycja na x
		 * @param y Pozycja na y
		 */
		public function spawn(x:int, y:int):void
		{
			var ex:Explosion = pool.getSprite() as Explosion;
			ex.emitterX = x;
			ex.emitterY = y;
			ex.start(0.1);
			play.addChild(ex);
			Starling.juggler.add(ex);
			ex.addEventListener(Event.COMPLETE, onComplete);
		}
		
		/**
		 * Kiedy animacja eksplozji sie zakonczy 
		 * wykonue sie ponizsza metoda
		 */
		private function onComplete(event:Event):void
		{
			var ex:Explosion = event.currentTarget as Explosion;
			Starling.juggler.remove(ex);
			
			if(pool != null)
				pool.returnSprite(ex);
		}
		
		/**
		 * Usuniecie managera eksplozj
		 */
		public function destroy():void
		{
			for(var i:int=0; i<pool.items.length; i++)
			{
				var ex:Explosion = pool.items[i];
				ex.dispose();
				ex = null;
			}
			pool.destroy();
			pool = null;
		}
	}
}