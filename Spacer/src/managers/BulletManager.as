package managers
{
	import com.leebrimelow.starling.StarlingPool;
	
	import core.Assets;
	
	import objects.Bullet;
	
	import states.Play;

	/**
	 * Klasa zarzadzajaca strzalami hero
	 * 
	 * @private play Stan gry w ktorym jest manager wykorzystywany
	 * @public bullets Tablica pociskow wystrzelonych przez hero
	 * @private pool Pula pociskow wystrzelonych
	 * @public count Ilosc klatek juz wyswietlonych
	 * @private kindOfShoot Rodzaj strzelania statku hero domyslnie pojedyncze (0)
	 */
	public class BulletManager
	{
		private var play:Play;
		public var bullets:Array;
		private var pool:StarlingPool;
		public var count:int = 0;
		private var kindOfShoot: uint =0;
		
		/**
		 * Konstruktor
		 * 
		 * @param play Stan gry w ktorym zostanie utworzony
		 */
		public function BulletManager(play:Play)
		{
			this.play = play;
			bullets = new Array();
			pool = new StarlingPool(Bullet, 100);
		}
		
		/**
		 * Update pozycji pociskow
		 */
		public function update():void
		{
			var b:Bullet;
			var len:int = bullets.length;
			
			for(var i:int=len-1; i>=0; i--)
			{
				b = bullets[i];
				b.y -= 25;
				
				//gdy wyleci poza ekran (na Y)
				if(b.y < 0)
					destroyBullet(b);
			}
			
			//jesli strzelam, to co 10 klatke pojawia sie pocisk
			if(play.fire && count%10 == 0)
			{
				//(0 - pojedyncze, 1-podwojne)
				if(kindOfShoot == 0)
				{
					fireSingle();
				}
				else
				{
					fireDouble();
				}
			}
			
			count ++;
		}
		
		/**
		 * Stworzenie 1 pocisku
		 */
		private function fireSingle():void
		{
			var b:Bullet = pool.getSprite() as Bullet;
			play.addChild(b);
			b.x = play.hero.x;
			b.y = play.hero.y - 10;
			bullets.push(b);
			
			//odglos strzalu
			Assets.shoot.play();
		}
		
		/**
		 * Stworzenie 2 pociskow ktore sa jednoczesnie wystrzelane
		 */
		private function fireDouble():void
		{
			var b:Bullet = pool.getSprite() as Bullet;
			play.addChild(b);
			b.x = play.hero.x - 15;
			b.y = play.hero.y - 15;
			bullets.push(b);
			
			b = pool.getSprite() as Bullet;
			play.addChild(b);
			b.x = play.hero.x + 15;
			b.y = play.hero.y - 15;
			bullets.push(b);
			
			//odglos strzalu
			Assets.shoot.play();
		}
		
		/**
		 * Zmiana rodzaju strzalow
		 */
		public function changeKindOfShootTo (kindOfShoot :uint):void
		{
			this.kindOfShoot = kindOfShoot;
		}
		
		/**
		 * Usuniecie pocisku
		 * 
		 * @param b Pocisk ktory ma zostac usuniety
		 */
		public function destroyBullet(b:Bullet):void
		{
			var len:int = bullets.length;
			
			for(var i:int=0; i<len; i++)
			{
				if(bullets[i] == b)
				{
					bullets.splice(i, 1);
					b.removeFromParent(true);
					pool.returnSprite(b);
				}
			}
		}
		
		/**
		 * Usuniecie calego managera strzalow
		 */
		public function destroy():void
		{
			pool.destroy();
			pool = null;
			bullets = null;
		}
	}
}