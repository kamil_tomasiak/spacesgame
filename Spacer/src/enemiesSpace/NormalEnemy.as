package enemiesSpace
{
	import com.leebrimelow.starling.StarlingPool;
	
	import interfaces.IEnemy;
	
	import objects.NormalEnemyDress;
	
	import starling.core.Starling;
	
	import states.Play;

	/**
	 * Klasa odpowiedzialna za normalnego wroga, robi update jego pozycji
	 * Implemetuje interfejs IEnemy
	 * 
	 * @private play Stan gry w ktorym jest tworzony ten wrog
	 * @public dress Kostium tej postaci
	 * @private pool Pula obiektow normalnego wroga
	 * @private speed Predkosc z jaka ma sie poruszac po ekranie
	 */
	public class NormalEnemy implements IEnemy
	{
		private var play:Play;
		public var dress:NormalEnemyDress;
		private var pool:StarlingPool;
		private var speed:int;
		
		/**
		 * Konstruktor klasy
		 * 
		 * @param play Stan w ktorym wrog powstanie
		 * @param postion Pozycja X w ktorej ma zostac utworzony
		 * @param speed Predkosc poruszania sie po scenie
		 */
		public function NormalEnemy(play:Play, position:int, speed:int)
		{
			this.play = play;
			this.pool = play.poolForNormalEnemyDress;
			this.speed = speed;
			
			spawn(position);
		}
		
		/**
		 * Metoda odpowiedzialna za tworzenie nowego obiektu wroga na scenie
		 * 
		 * @param position Pozycja w na ktorej ma sie pojawic (X)
		 */
		private function spawn(position:int):void
		{
			dress = pool.getSprite() as NormalEnemyDress;
			//Starling.juggler.add(dress); - do animacji
			dress.y = -50;
			dress.x = position;
			play.addChild(dress);
		}
		
		/**
		 * Update pozycji statku wroga
		 * 
		 * @param positionInTable Pozycja w tablicy wrogow w stanie Play
		 */
		public function update():void
		{
			dress.y += speed;
			
			//jesli statek wylecial poza ekran na Y
			if (dress.y > 640)
			{
				destroyDress();
			}
		}
		
		/**
		 * Usuwanie statku
		 * 
		 * @param positionInTable Pozycja w tablicy wrogow w stanie play
		 */
		public function destroy():void
		{
			dress.removeFromParent(true);
			pool.returnSprite(dress);
			
			//usuniecie obiektu z tablicy wrogow
			var enemiesNormal:Array = play.enemiesNormal;
			for(var i:int=enemiesNormal.length-1; i>=0; i--)
			{
				if( this == enemiesNormal[i] )
				{
					enemiesNormal.splice(i, 1);
				}
			}

		}
		
		/**
		 * Przenosi do usuniecia calego obiektu
		 * Uzywana przez shooter enemy
		 */
		public function destroyDress():void
		{
			destroy();
		}
		
		/**
		 * Zmniejszanie ilosci zycia postaci
		 * Ta akurat ma tylko jedno zycie dlatego nie wykorzystywana metoda
		 */
		public function decreaseLives():void
		{
		}
		
		/**
		 * Zwraca ilosci zycia postaci
		 * Ta akurat ma tylko jedno zycie dlatego nie wykorzystywana metoda
		 */
		public function getCountLives():uint
		{
			return 0;
		}
		
		/**
		 * Pozycja na scenie
		 * 
		 * @return zwraca pozycje X
		 */
		public function getDressX():int
		{
			return dress.x;
		}
		
		/**
		 * Pozycja na scenie
		 * 
		 * @return zwraca pozycje Y
		 */
		public function getDressY():int
		{
			return dress.y;
		}
		
		/**
		 * Pivot X obiektu
		 * 
		 * @return zwraca pivot na X
		 */
		public function getDressPivotX():int
		{
			return dress.pivotX;
		}
		
		/**
		 * Pivot Y obiektu
		 * 
		 * @return zwraca pivot na Y
		 */
		public function getDressPivotY():int
		{
			return dress.pivotY;
		}
		
		/**
		 * Sprawdza czy jest zestrzelony juz statek 
		 * <- w shooter enemy
		 * tutaj narazie nie wykorzystywane
		 */
		public function isItKilled():Boolean
		{
			return false;
		}
		
	}
}