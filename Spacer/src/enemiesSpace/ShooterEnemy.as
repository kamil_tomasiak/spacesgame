package enemiesSpace
{
	import com.leebrimelow.starling.StarlingPool;
	
	import interfaces.IEnemy;
	
	import objects.BulletEnemy;
	import objects.ShooterEnemyDress;
	
	import starling.display.DisplayObject;
	
	import states.Play;

	/**Klasa zazadzajca wrogiem strzelajacym
	 * Implemetuje interfejs IEnemy
	 * 
	 * @private play Stan gry w ktorym jest tworzony ten wrog
	 * @private pool Pula obiektow strzelajacego wroga
	 * @private speed Predkosc z jaka ma sie poruszac po ekranie
	 * @public dress Kostium tej postaci
	 * @private lives Ilosc zyc tej postaci (2)
	 * @public bullets Tablica pociskow wystrzelonych przez statek
	 * @private poolForBullets Pula pociskow wystrzelonych
	 * @private count Ilosc klatek juz wyswietlonych (do testow, update bullet)
	 * @private isKilled Czy statek zostal juz zestrzelony
	 */
	public class ShooterEnemy implements IEnemy
	{
		private var play:Play;
		private var pool:StarlingPool;
		private var speed:int;
		public var dress:ShooterEnemyDress;
		private var lives:uint;
		public var bullets:Array;
		private var poolForBullets:StarlingPool;
		private var count :int = 0;
		private var isKilled :Boolean = false;
		
		/**
		 * Konstruktor klasy
		 * 
		 * @param play Stan w ktorym wrog powstanie
		 * @param postion Pozycja X w ktorej ma zostac utworzony
		 * @param speed Predkosc poruszania sie po scenie
		 */
		public function ShooterEnemy(play:Play, position:int, speed:int)
		{
			this.play = play;
			this.pool = play.poolForShooterEnemyDress;
			this.speed = speed;
			lives = 2;
			
			//tablica strzalow
			bullets = new Array;
			
			poolForBullets = new StarlingPool(BulletEnemy, 50);
			
			spawn(position);
		}
		
		/**
		 * Metoda odpowiedzialna za tworzenie nowego obiektu wroga na scenie
		 * 
		 * @param position Pozycja w na ktorej ma sie pojawic (X)
		 */
		private function spawn(position:int):void
		{
			dress = pool.getSprite() as ShooterEnemyDress;
			//Starling.juggler.add(dress); - do animacji
			dress.y = -50;
			dress.x = position;
			dress.alpha = 1;
			play.addChild(dress);
		}
		
		/**
		 * Update pozycji statku wroga
		 * 
		 * @param positionInTable Pozycja w tablicy wrogow w stanie Play
		 */
		public function update():void
		{
			if (!isKilled) 
			{
				updateBullet();
				
				dress.y += speed;
			
				if (dress.y > 640)
				{
					destroyDress();
					destroy();
				}
			}
			else
			{
				// jesli jest zabity i ilosc strzalow jest zero to usuwam ten obiekt
				if (bullets.length ==0)
				{
					destroy();
				} 
				else 
				{
					updateBullet();	
				}
			}
			
		}
		
		/**
		 * Update strzalow tego wroga
		 */
		private function updateBullet():void
		{
			var len:int = bullets.length;
			for (var i:int = len-1; i>=0; i--)
			{
				bullets[i].y += speed+5;
				
				//gdy jest po za ekranem usuwam
				if(bullets[i].y > 640)
				{
					destroyBullet(i, bullets[i]);
				}
			}
			
			//co 30 klatke stworzy sie nowy pocisk, jesli statek zyje
			if(!isKilled && count %50 == 0) 
			{
				createBullet();	
			}
			count++;
		}
		
		/**
		 * Tworzenie nowego pocisku
		 */
		private function createBullet():void
		{
			var dressBullet :BulletEnemy = poolForBullets.getSprite() as BulletEnemy;
			play.addChild(dressBullet);
			dressBullet.y = dress.y + 10;
			dressBullet.x = dress.x;
			
			bullets.push(dressBullet);
		}
		
		/**
		 * Usuniecie pocisku
		 * 
		 * @param positionInTable Pozycja pocisku w tablicy pociskow
		 * @param dressBullet Kostium ktory ma zostac usuniety ze sceny
		 */
		public function destroyBullet(positionInTable:uint, dressBullet:BulletEnemy):void
		{
			bullets.splice(positionInTable, 1);
			dressBullet.removeFromParent(true);
			poolForBullets.returnSprite(dressBullet);	
		}
		
		/**
		 * Usuniecie stroju wroga
		 */
		public function destroyDress():void
		{
			isKilled = true;
			dress.removeFromParent(true);
			pool.returnSprite(dress);
		}
		
		/**
		 * Usuniecie calego obiektu
		 */
		public function destroy():void
		{		
			if(!isKilled)
			{
				destroyDress();
			}
			//usuniecie obiektu z tablicy strzelajacych wrogow
			var enemiesShooter:Array = play.enemiesShooter;
			for(var i:int=enemiesShooter.length-1; i>=0; i--)
			{
				if( this == enemiesShooter[i] )
				{
					enemiesShooter.splice(i, 1);
				}
			}
			
			poolForBullets.destroy();
			poolForBullets = null;
			bullets = null;
		}
		
		/**
		 * Zmniejszanie ilosci zycia postaci
		 */
		public function decreaseLives():void
		{
			lives--;
			//staje sie przezroczysty
			dress.alpha = 0.5;
		}
		
		/**
		 * Zwraca ilosci zycia postaci
		 */
		public function getCountLives():uint
		{
			return lives;
		}
		
		/**
		 * Pozycja na scenie
		 * 
		 * @return zwraca pozycje X
		 */
		public function getDressX():int
		{
			return dress.x;
		}
		
		/**
		 * Pozycja na scenie
		 * 
		 * @return zwraca pozycje Y
		 */
		public function getDressY():int
		{
			return dress.y;
		}
		
		/**
		 * Pivot X obiektu
		 * 
		 * @return zwraca pivot na X
		 */
		public function getDressPivotX():int
		{
			return dress.pivotX;
		}
		
		/**
		 * Pivot Y obiektu
		 * 
		 * @return zwraca pivot na Y
		 */
		public function getDressPivotY():int
		{
			return dress.pivotY;
		}
		
		/**
		 * Sprawdza czy statek jest juz zestrzelony
		 * 
		 * @return true jesli zestrzelony
		 */
		public function isItKilled():Boolean
		{
			return isKilled;
		}
	}
}