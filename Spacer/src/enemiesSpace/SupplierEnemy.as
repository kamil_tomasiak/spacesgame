package enemiesSpace
{
	import com.leebrimelow.starling.StarlingPool;
	
	import interfaces.IEnemy;
	
	import bonuses.AttackBonus;
	import bonuses.CoinBonus;
	import bonuses.FasterHeroBonus;
	import bonuses.HeartBonus;
	import objects.SupplierEnemyDress;
	
	import starling.display.DisplayObject;
	
	import states.Play;
	
	/**
	 * Obsluga wroga dostarczajacego
	 * 
	 * @private play Stan gdzie dziala
	 * @private speed predkosc poruszania
	 * @private pool Pula obiektow strojow dla tego wroga
	 * @private dress Textura obiektu
	 * @private isKilled Zostal juz zestrzelony czy nie
	 * @private lives Ilosc dostepnych zyc
	 * @private kindOfBonus Jaki rodzaj bonusu przewozi
	 */
	public class SupplierEnemy implements IEnemy
	{
		private var play:Play;
		private var speed:int;
		private var pool:StarlingPool;
		private var dress:SupplierEnemyDress;
		private var isKilled: Boolean = false; 
		private var lives : uint = 3;
		private var kindOfBonus:int;
		
		/**
		 * Konstruktor
		 * 
		 * @param play Stan rozgrywki
		 * @param position Pozycja na scenie
		 * @param spped Predkosc poruszania
		 * @param kindOfBonus Jaki bonus przewozi
		 */
		public function SupplierEnemy(play : Play, positionY: int, speed : int, kindOfBonus: int)
		{
			this.play = play;
			pool = play.poolForSupplierEnemyDress;
			this.speed = speed;
			this.kindOfBonus = kindOfBonus;
			
			createNew(positionY);
		}
		
		/**
		 * Utworzenie nowego obiektu na scenie
		 * 
		 * @param positionX Pozycja na scenie
		 */
		private function createNew(positionY:int):void
		{
			dress = pool.getSprite() as SupplierEnemyDress;
			dress.y = positionY;
			dress.x = -50;
			dress.alpha = 1;
			play.addChild(dress);
		}

		/**
		 * Update statku
		 */
		public function update():void
		{
			dress.x += speed;
			
			if(dress.x > 740)
			{
				destroy();
			}
		}
		
		/**
		 * Usuniecie obiektu z tablicy w Play, stroju
		 */
		public function destroy():void
		{
			dress.removeFromParent(true);
			pool.returnSprite(dress);
			
			//usuniecie obiektu z tablicy wrogow
			var enemies:Array = play.enemiesSupplier;
			for(var i:int=enemies.length-1; i>=0; i--)
			{
				if( this == enemies[i] )
				{
					enemies.splice(i, 1);
				}
			}
		}
		
		public function decreaseLives():void
		{
			lives--;
			//staje sie przezroczysty
			dress.alpha -= 0.33;
		}
		
		public function getCountLives():uint
		{
			return lives;
		}
		
		/**
		 * Usunie obiekt ale wczesniej na miejscu tej postaci
		 * stworzy to co ona ma w srodku(monety, dodatkowe zycie..)
		 */
		public function destroyDress():void
		{
			
			switch(kindOfBonus) 
			{
				case 1 :
					var heartBonus :HeartBonus = new HeartBonus(play, dress.x, dress.y);
					play.bonusesTable.push(heartBonus);
					play.addChild(heartBonus);
					break;
				case 2 :
					var bonusCoin :CoinBonus = new CoinBonus(play, dress.x, dress.y);
					play.bonusesTable.push(bonusCoin);
					play.addChild(bonusCoin);
					break;
				case 3 :
					var attackBonus :AttackBonus = new AttackBonus(play, dress.x, dress.y);
					play.bonusesTable.push(attackBonus);
					play.addChild(attackBonus);
					break;
				case 4 :
					var fasterHeroBonus :FasterHeroBonus = new FasterHeroBonus(play, dress.x, dress.y);
					play.bonusesTable.push(fasterHeroBonus);
					play.addChild(fasterHeroBonus);
					break;
			}
			
			destroy();
		}
		
		public function getDressX():int
		{
			return dress.x;
		}
		
		public function getDressY():int
		{
			return dress.y;
		}
		
		public function getDressPivotX():int
		{
			return dress.pivotX;
		}
		
		public function getDressPivotY():int
		{
			return dress.pivotY;
		}
		
		/**
		 * Sprawdza czy samolot zestrzelony
		 * @return true jesli zostal zestrzelony
		 */
		public function isItKilled():Boolean
		{
			return false;
		}
	}
}